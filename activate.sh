#!/usr/bin/env bash
# ~/.config/bash/activate.sh
#
# Replace '.bash_profile' and '.bashrc' to source all of '.config/bash/'
# Existing '.bash_profile' and '.bashrc' are preserved in '.config/bash/'

bp='bash_profile'
brc='bashrc'
config='.config/bash'

mv ${HOME}/.${bp} $HOME/${config}/${bp}
mv ${HOME}/.${brc} $HOME/${config}/${brc}

echo '# Do .bashrc' > ${HOME}/.${bp}
echo '[[ -f ~/.bashrc ]] && . ~/.bashrc' >> ${HOME}/.${bp}
chmod 600 ${HOME}/.${bp}

echo '# Do nothing unless running interactively.' > ${HOME}/.${brc}
echo '[[ $- != *i* ]] && return' >> ${HOME}/.${brc}
echo '' >> ${HOME}/.${brc}
echo 'source ${HOME}/.config/bash/load' >> ${HOME}/.${brc}
chmod 600 ${HOME}/.${brc}
